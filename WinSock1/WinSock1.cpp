#include "stdafx.h"
#include <iostream>
#include <sstream>
#include <string>
#include <conio.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include "vld.h"
#include <fstream>
#include <vector>
#include <map>
#pragma comment(lib, "Ws2_32.lib")
using namespace std;
static int currentID = 0;
bool access = false;
class Server {
	public:
		 bool winsockStarted;
		 int status;
		 struct addrinfo hints;
		 struct addrinfo* addr;
		 struct sockaddr_storage their_addr;
		 SOCKET serv;

	Server();
    ~Server();
    bool Start(const char *port);
    void Stop();
};
Server::~Server()
{
	closesocket(serv);
	delete &hints;
	delete addr;
	delete &their_addr;
}
Server::Server()
    : serv(INVALID_SOCKET), winsockStarted(false)
{
    WSADATA WSAData = {0};
    int status = WSAStartup(MAKEWORD(2, 0), &WSAData);
    if (status != 0)
        std::cout << "[ERROR]: " << status << " Unable to start Winsock." << std::endl;
    else
        winsockStarted = true;
};
bool Server::Start(const char *port)
{
	Stop();

    struct addrinfo hints = {0};
    struct addrinfo *res = NULL;

    hints.ai_flags = AI_PASSIVE;
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    int status = getaddrinfo(NULL, port, &hints, &res);
    if (status != 0)
    {
        std::cout << "[ERROR]: " << status << " Unable to get address info for Port " << port << "." << std::endl;
        return false;
    }

    SOCKET newsock = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if (newsock == INVALID_SOCKET)
    {
        std::cout << "[ERROR]: " << WSAGetLastError() << " Unable to create Socket." << std::endl;
        freeaddrinfo(res);
        return false;
    }

    if (bind(newsock, res->ai_addr, res->ai_addrlen) == SOCKET_ERROR)
    {
        std::cout << "[ERROR]: " << WSAGetLastError() << " Unable to bind Socket." << std::endl;
        freeaddrinfo(res);
        closesocket(newsock);
        return false;
    }

    freeaddrinfo(res);

    if (listen(newsock, SOMAXCONN) == SOCKET_ERROR)
    {
        std::cout << "[ERROR]: " << WSAGetLastError() << " Unable to Listen on Port " << port << "." << std::endl;
        closesocket(newsock);
        return false;
    }

    serv = newsock;
    return true;
}

void Server::Stop()
{
    if (serv != INVALID_SOCKET)
    {
        closesocket(serv);
        serv = INVALID_SOCKET;
    }
};
//SOCKET Listen_socket;
class Client
{
public:
	int ID;
	int clientLen;
	SOCKET Client_socket;
	sockaddr_in client_addr;
	stringstream recvstr, response,response_body;
	map <string,string> parseres;
	Client()
	{
		ID = currentID++;
		memset (&(client_addr.sin_zero),0,8);
		clientLen = sizeof(client_addr);
	}
};
struct Params
{
	Client *client;
	vector<Client*>*allClients;
	Params (Client*& _client, vector<Client*>* _allClient)
	{
		client = _client;
		allClients = _allClient;
	}
};
void NewClientConnected(void*);
//SOCKET Server;
	std::map<std::string, std::string> parseKV(const std::string &sz)
	{
    std::map<std::string, std::string> ret;
    std::string key;
    std::string value;
	if (sz.length() == 1)
	{
		key = "Telnet connection";
		value = sz;
		ret[key]=value;
		return ret;
	}
    const char *s=sz.c_str();
	int coliter=0;
    while(*s) {
		while (coliter == 0)
		{
		////////////////////////METHOD//////////////////////////////
		while ((*s!=' '))
		{
			key.push_back(*s);
			++s;
		}
		if (key == "Client")
		{key = "Client connection"; value = sz; ret[key]=value; return ret;}
		       if(!*s) break;
			   s++;
		while (coliter==0 && *s && s[1]!=' ')
		{
			value.push_back(*s);
            ++s;
		}
		 ret[key]=value;
        key.clear(); value.clear();
		s+=2;
		///////////////////////PROTOCOL/////////////////////////////
		while (coliter==0 && (*s!= '/'))
		{
			key.push_back(*s);
			++s;
		}
		       if(!*s) break;
			   s++;
		while (coliter==0 && *s && *s!='\n')
		{
			value.push_back(*s);
            ++s;	
		}
		 ret[key]=value;
        key.clear(); value.clear();
		coliter = 1;
		//++s;}
		////////////////////////////////////////////////////////////////
		}
        while(*s && *s!=':' && s[1]!=' ') {
            key.push_back(*s);
            ++s;
        }
        // if we quit due to the end of the string exit now
        if(!*s) break;
        // skip the ": "
        s+=2;
        // parse the value
        while(*s && *s!='\n') {
            value.push_back(*s);
            ++s;
        }
        ret[key]=value;
        key.clear(); value.clear();
        // skip the newline
        ++s;
    }
    return ret;
}

int main()
{
	setlocale(LC_ALL, "rus");
	SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
	//Server Serv= new Server();
	vector<Server*>Servers;
	Servers.push_back(new Server());
	Servers.back()->Start("55555");
	printf("Start server...\n");
	/*if(listen(Servers.back()->Serv, SOMAXCONN)==SOCKET_ERROR)
	{
		cerr<<"Listen Error: "<<WSAGetLastError()<<"\n";
		closesocket(Servers.back()->Serv);
		WSACleanup();
		return 1;
	}
	bind(Servers.back()->Serv,Servers.back()->addr->ai_addr,(int)Servers.back()->addr->ai_addrlen);*/
	vector<HANDLE> clientsThreads;
    vector<Client*> clients;
	for (;;){
	cout << "Waiting..." << endl;
	clients.push_back(new Client());
	//cout << "Waiting..." << endl;
	clients.back()->Client_socket = accept(Servers.back()->serv, (struct sockaddr *)&clients.back()->client_addr, &clients.back()->clientLen);
            clientsThreads.push_back(CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)NewClientConnected, new Params(clients.back(), &clients), 0, NULL));
            access = false;
         /*   while(true)
            {
                if(access == true)*/
                {
                    clients.push_back(new Client());
                   // break;
                }
           // }
        }
	
    WSACleanup();
   return 0;
}
void NewClientConnected(void* _params)
{
	 const int sob = 100;
	 char recvbuff [sob];
	 char sendbuff [1024];
	 int ret=1;
     Params* params = (Params*)_params;
     printf("New client connection opens...\r\n");
     vector<Client*>::iterator iter;
	/* do
	 {*/
     do 
     {
		/* ULONG ulBlock;
		 ulBlock = 1;
		 ioctlsocket(params->client->Client_socket, FIONBIO, &ulBlock);*/
		 ret = recv(params->client->Client_socket, recvbuff, sob-1, 0);
		 recvbuff[ret]='\0';
		 params->client->recvstr<<recvbuff;
		 if (ret == -1)
					break;
	 } while (ret==sob-1);
		 params->client->response_body<<"<title>Hello world</title>\n"
        << "<h1>test page!!!</h1>\n"
        << "<p>This is the test page made for practice in CHISW...</p>\n"
        << "<h2>Request headers</h2>\n"
        << "<pre>" << params->client->recvstr.str() << "</pre>\n"
        << "<em><small>Test C++ Http Server</small></em>\n";
				params->client->response << "HTTP/1.1 200 OK\r\n"
        << "Version: HTTP/1.1\r\n"
        << "Content-Type: text/html; charset=cp-1251\r\n"
		<< "Connection: close \r\n"
        << "Content-Length: " << params->client->response_body.str().length()
        << "\r\n\r\n"
        << params->client->response_body.str();
        params->client->parseres = parseKV(params->client->recvstr.str());
		if (params->client->parseres.begin()->first == "Telnet connection")
		{
				string telnrecv = "This is what you've type:";
				telnrecv+=params->client->recvstr.str();
				send(params->client->Client_socket, telnrecv.c_str(),
       telnrecv.length(), 0);
		}
		else if (params->client->parseres.begin()->first == "Client connection")
		{
			stringstream telnrecv;
			telnrecv<<"This is what you've type: "<< params->client->recvstr.str();
			//telnrecv+=recvbuff;
				send(params->client->Client_socket, telnrecv.str().c_str(),
       telnrecv.str().length(), 0);
		}
		else
		ret = send(params->client->Client_socket, params->client->response.str().c_str(),
       params->client-> response.str().length()+1, 0);
				params->client->response.clear();
				params->client->response_body.clear();
				//closesocket(params->client->Client_socket);
		
     }
